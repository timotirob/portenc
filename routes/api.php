<?php

    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('/categorie/{categorie}/competences', 'CategorieController@competences');
        Route::resource('/categorie', 'CategorieController');
        Route::resource('/competence', 'CompetenceController');
    });
